﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloWorld.Models {
    public class User {

        public int UserID { get; set; }

        public string Name { get; set; }

        public string Mail { get; set; }

        public string Pass { get; set; }

        public bool Active { get; set; }

        public DateTime RegisterDate { get; set; }

        public virtual ICollection<Article> Articles { get; set; }

    }
}
