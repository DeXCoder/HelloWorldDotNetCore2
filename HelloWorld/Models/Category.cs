﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelloWorld.Models {
    public class Category {

        public int CategoryID { get; set; }

        public string Name { get; set; }

        public string Icon { get; set; }

        public virtual ICollection<Article> Articles { get; set; }
    }
}
