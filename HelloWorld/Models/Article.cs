﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HelloWorld.Models {
    public class Article {

        public int ArticleID { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        [Required]
        public int CategoryID { get; set; }
        public virtual Category Category { get; set; }

        public int UserID { get; set; }
        public virtual User User { get; set; }


    }
}
