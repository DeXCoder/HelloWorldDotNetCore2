﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelloWorld.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HelloWorld.Controllers {
    public class HomeController : Controller {
        private readonly ModelContext db = new ModelContext();

        public IActionResult Index() {
            if (db.Database.EnsureCreated() == false) {
                using (var context = new ModelContext()) {
                    context.Database.Migrate();
                }
            }


            return View(db.Articles.ToList());
        }
    }
}